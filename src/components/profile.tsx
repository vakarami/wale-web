import React from "react";
import { Link } from "react-router-dom";
import { getCurrentUser } from "../services/authService";
import { setPageTitle } from "../utils/general";

const Profile: React.FC = () => {
  const user = getCurrentUser();
  setPageTitle(user?.name + " Profile");
  return (
    <React.Fragment>
      <h1>Your Profile</h1>
      <p>{user?.name}</p>
      <p>{user?.email}</p>
      <button className="btn btn-secondary">Edit Profile</button>
      <Link className="btn btn-secondary m-2" to={"/user/ads/" + user?._id}>
        Your Ads
      </Link>
    </React.Fragment>
  );
};

export default Profile;
