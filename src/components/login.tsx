import Joi from "joi";
import Form, { FormProps, FormState } from "./common/form";
import { login } from "../services/authService";
import { toast } from 'react-toastify';
import React from "react";
import { Link } from "react-router-dom";
import { setPageTitle } from "../utils/general";

const emptyUser = {
  email: "",
  password: "",
};
const emailOptions = { tlds: { allow: false } };//allow invalid mail providers as well
class Login extends Form<FormProps, FormState> {
  state = {
    data: emptyUser,
    errors: {},
  };
  schema = {
    email: Joi.string().email(emailOptions).label("This"),
    password: Joi.string().label("Password"),
  };

  doSubmit = async () => {
    const result = await login(this.state.data.email, this.state.data.password);
    if(result){
      const { state } = this.props.location; //set in protectedRoute
      window.location.href = state ? state as string : "/"; //to reload the page
    }
    else{
      toast.error("Unable to login, Username or Password is not correct")
    }
  };;
  render() {
    setPageTitle("Login to")
    return (
      <div className="m-auto" style={{ maxWidth: "400px" }}>
        <h1>Login to E-Wall</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("email", "Email")}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Login")}
        </form>
        <h6 className="mt-5">
          Not a user? Register from <Link to="/user/register">here</Link>
        </h6>
      </div>
    );
  }
}

export default Login;
