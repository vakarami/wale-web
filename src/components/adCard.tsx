import React from "react";
import { Link } from "react-router-dom";
import { AdType } from "../common/types";
import { getRelativeTime } from "../utils/general";

const baseUrl = process.env.REACT_APP_BACKEND;

interface Props{
  details: AdType;
}
const AdCard: React.FC<Props> = (props) => {
  const { title, price, city, currency, createdAt, _id, photos } = props.details;
  return (
    <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
      <div className="card border border-1 rounded-top" >
        {renderImg(photos[0], title)}
        <div className="card-body d-flex flex-column">
          <h5 className="card-title text-truncate">{title}</h5>
          <div className="card-text">
            {currency} {price}
          </div>
          <div className="card-text">
            <small className="text-muted">
              {getRelativeTime(new Date(createdAt))} in {city}
            </small>
          </div>
          <Link to={"/ads/" + _id} className="stretched-link" />
        </div>
      </div>
    </div>
  );
}

function renderImg(photo: string, title: string) {
  if (photo) {
    return (
      <img src={baseUrl + "/" + photo} className=" rounded m-1" alt={title} style={{maxHeight:"240px"}}/>
    );
  }
  return (
    <div
      className="icon-demo rounded d-flex align-items-center justify-content-center p-3 py-6 m-1"
      // style="font-size: 10em"
      role="img"
      aria-label="Image fill - large preview"
      style={{ backgroundColor: "rgb(0 0 0 / 15%)" }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="240"
        fill="currentColor"
        className="bi bi-image-fill"
        viewBox="0 0 16 16"
      >
        <defs>
          <linearGradient
            id="linear-gradient"
            gradientUnits="userSpaceOnUse"
            x1="3.002"
            y1="5.5"
            x2="6.002"
            y2="5.5"
          >
            <stop offset="0" stopColor="#68686f" stopOpacity="1" />
            <stop offset="1" stopColor="#8e8e93" stopOpacity="1" />
          </linearGradient>
        </defs>
        <path
          id="Path"
          d="M6.002 5.5 C6.002 6.328 5.33 7 4.502 7 3.674 7 3.002 6.328 3.002 5.5 3.002 4.672 3.674 4 4.502 4 5.33 4 6.002 4.672 6.002 5.5 Z"
          fillOpacity="1"
          fill="url(#linear-gradient)"
          stroke="none"
        />
        <defs>
          <linearGradient
            id="linear-gradient-1"
            gradientUnits="userSpaceOnUse"
            x1="0.002"
            y1="8"
            x2="16.002"
            y2="8"
          >
            <stop offset="0" stopColor="#68686f" stopOpacity="1" />
            <stop offset="1" stopColor="#8e8e93" stopOpacity="1" />
          </linearGradient>
        </defs>
        <path
          id="Path-1"
          d="M2.002 1 C1.472 1 0.963 1.211 0.588 1.586 0.213 1.961 0.002 2.47 0.002 3 L0.002 13 C0.002 13.53 0.213 14.039 0.588 14.414 0.963 14.789 1.472 15 2.002 15 L14.002 15 C14.532 15 15.041 14.789 15.416 14.414 15.791 14.039 16.002 13.53 16.002 13 L16.002 3 C16.002 2.47 15.791 1.961 15.416 1.586 15.041 1.211 14.532 1 14.002 1 Z M14.002 2 C14.267 2 14.522 2.105 14.709 2.293 14.897 2.48 15.002 2.735 15.002 3 L15.002 9.5 11.225 7.553 C11.033 7.457 10.8 7.494 10.648 7.646 L6.938 11.356 4.278 9.584 C4.08 9.452 3.817 9.478 3.648 9.646 L1.002 12 1.002 3 C1.002 2.448 1.45 2 2.002 2 Z"
          fillOpacity="1"
          fill="url(#linear-gradient-1)"
          stroke="none"
        />
      </svg>
    </div>
  );
}

export default AdCard;
