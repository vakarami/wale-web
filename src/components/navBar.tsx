import React, { useState } from "react";
import { Link, NavLink, useHistory } from "react-router-dom";
import { getCurrentUser } from "../services/authService";

const NavBar: React.FC = () => {
  const [searchText, setSearchText] = useState("");
  const history = useHistory();
  return (
    <nav className="navbar navbar-expand-md navbar-light bg-light mb-3">
      <div className="container-fluid">
        <Link className="navbar-brand" to="/">
          E-Wall
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-md-0">
            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="/ads"
              >
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/ads/new">
                New Ad
              </NavLink>
            </li>
            {renderUserOrLogin()}
          </ul>
          <form className="d-flex">
            <input
              className="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
            />
            <button className="btn btn-outline-secondary" type="submit" onClick={search}>
              Search
            </button>
          </form>
        </div>
      </div>
    </nav>
  );
  function search(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    e.preventDefault();
    history.push("/ads?search=" + searchText);
  }
};

function renderUserOrLogin() {
  const user = getCurrentUser();
  if (user) {
    return (
      <li className="nav-item dropdown">
        <div
          className="nav-link dropdown-toggle link"
          id="navbarDropdown"
          role="button"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          {user.name}
        </div>
        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
          <li>
            <NavLink className="dropdown-item" to="/user/me">
              Profile
            </NavLink>
          </li>
          <li>
            <NavLink className="dropdown-item" to={"/user/ads/" + user._id}>
              Your Ads
            </NavLink>
          </li>
          <li>
            <hr className="dropdown-divider" />
          </li>
          <li>
            <NavLink className="dropdown-item" to="/user/logout">
              Logout
            </NavLink>
          </li>
        </ul>
      </li>
    );
  } else {
    return (
      <React.Fragment>
      <li className="nav-item">
        <NavLink className="nav-link text-capitalize" to="/user/register">
          Register
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link text-capitalize" to="/user/login">
          Login
        </NavLink>
      </li>
      </React.Fragment>
    );
  }
}
export default NavBar;
