import React from "react";
import Joi from "joi";
import { emptyUser, UserType } from "../common/types";
import Form, { FormProps, FormState } from "./common/form";
import { Link } from "react-router-dom";
import { register } from "../services/authService";
import { toast } from "react-toastify";
import { setPageTitle } from "../utils/general";

export interface Props extends FormProps {}

export interface State extends FormState {
  data: UserType;
}

const emailOptions = { tlds: { allow: false } }; //allow invalid mail providers as well
class RegisterForm extends Form<Props, State> {
  state = { data: emptyUser, errors: {} };
  schema = {
    _id: Joi.string().allow(null, ""),
    name: Joi.string().required().label("Name").min(3),
    email: Joi.string().required().email(emailOptions).label("This").min(5),
    password: Joi.string().required().label("Password").min(8),
    repassword: Joi.any()
      .valid(Joi.ref("password"))
      .required()
      .label("Confirm Password")
      .messages({ "any.only": "Password and the Confirmation don't match" }),
  };
  doSubmit = async () => {
    const result = await register(this.state.data);
    if (!result.error) {
      const { state } = this.props.location; //set in protectedRoute
      window.location.href = state ? (state as string) : "/"; //to reload the page
    } else {
      let message = result.error;
      if (message.includes("duplicate") && message.includes("email")) message = "Email is already registerd";
      toast.error("Unable to register: " + message);
    }
  };

  render() {
    setPageTitle("Register as new user to")
    return (
      <div className="m-auto" style={{ maxWidth: "400px" }}>
        <h1>Register as a new user</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("name", "Name")}
          {this.renderInput("email", "Email")}
          {this.renderInput("password", "Password", "password")}
          {this.renderInput("repassword", "Confirm Password", "password")}
          {this.renderButton("Register")}
        </form>
        <h6 className="mt-5">
          Already a user? Login from <Link to="/user/login">here</Link>
        </h6>
      </div>
    );
  }
}

export default RegisterForm;
