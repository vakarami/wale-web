import { getByUser } from "../services/adService";
import { toast } from "react-toastify";
import { SimpleAds } from "./ads";
import withLoading from "./common/withLoading";
import { getCurrentUser, getUser } from "../services/authService";

//state sharing doesn't work if we exstend Ads since it has a higer order component
class UserAds extends SimpleAds {
  async loadAds(refresh = true) {
    this.props.onLoading();
    const userId = this.props.match.params.id;
    try {
      const resposne = await getByUser(userId, this.getPagingQuery(refresh));
      if (refresh) {
        this.loadUser(userId); //load user deatails only in first load
      }
      this.setData(resposne.data, refresh);
    } catch (error) {
      toast.warn("Can't load ads for user :" + userId);
    }
    this.props.onLoadingDone();
  }

  loadUser(userId: string) {
    const currentUser = getCurrentUser();
    if (currentUser?._id === userId) {
      this.setState({ title: "Ads posted by You" });
    } else {
      getUser(userId).then((value) => {
        this.setState({ title: "Ads posted by " + value?.name });
      });
    }
  }
}

export default withLoading(UserAds);
