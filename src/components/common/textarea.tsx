import React from "react";

interface Props {
  name: string;
  label: string;
  value: string;
  error: string;
  onChange: (e:React.ChangeEvent<HTMLTextAreaElement>)=>void
}

const Textarea: React.FC<Props> = ({ name, label, error, ...rest }) => {
  return (
    <div className="form-group mt-2">
      <label htmlFor={name}>{label}</label>
      <textarea
        {...rest}
        name={name}
        id={name}
        className="form-control"
        style={{ minHeight: "150px" }}
      />
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Textarea;
