import React from "react";

const withLoading = (Component: typeof React.Component | React.FC) => {
  return class WithLoading extends React.Component {
    state = {
      loading: false,
    };
    onLoadingDone = () => {
      this.setState({ loading: false });
    };
    onLoading = () => {
      this.setState({ loading: true });
    };
    isLoading = (): boolean =>{
      return this.state.loading;
    }
    render() {
      return (
        <React.Fragment>
          <Component
            {...this.props}
            onLoadingDone={this.onLoadingDone}
            onLoading={this.onLoading}
            isLoading={this.isLoading}
          />
          {this.state.loading && (
            <div className="row">
              <div className=" spinner-border mx-auto my-5" role="status" />
            </div>
          )}
        </React.Fragment>
      );
    }
  };
}

export default withLoading;
