import React, { useState } from "react";
import { upload } from "../../services/photoService";
import { toast } from "react-toastify";

interface Props {
  photos: string[];
  onUpload: (_images: string[]) => void;
}
const ImageUpload: React.FC<Props> = ({ photos, onUpload }) => {
  const [imageUrls, setImageUrls] = useState([] as string[]);
  const [progress, setProgress] = useState(0);

  const handleUpload: React.ChangeEventHandler<HTMLInputElement> = async (event) => {
    setProgress(0);
    const files = event.target.files || [];
    const imageUrls: string[] = [];
    const rawFiles: File[] = [];
    for (let i = 0; i < Math.min(5, files.length); i++) {
      imageUrls.push(URL.createObjectURL(files[i]));
      rawFiles.push(files[i]);
    }
    setImageUrls(imageUrls);

    const response = await upload(rawFiles, (event) => {
      setProgress(Math.round((100 * event.loaded) / event.total));
    });
    if (response.error) {
      return toast.warn("Can't Upload photos " + response.error);
    }
    onUpload(response.data || []);
  };

  const urls = imageUrls.length ? imageUrls : photos;
  return (
    <React.Fragment>
      <div className="mt-2">
        <label htmlFor="formFileMultiple" className="form-label">
          Please Select photos (max 5 photos)
        </label>
        <input className="form-control" type="file" id="formFileMultiple" accept="image/*" onChange={handleUpload} multiple />
      </div>
      <div className="progress mt-2 mb-2">
        <div className="progress-bar" role="progressbar" aria-valuemin={0}  aria-valuemax={100} style={{width:progress+"%"}} ></div>
      </div>
      <div>
        {urls.map((imageUrl, i) => (
          <img className="img-thumbnail rounded" key={i} src={imageUrl} alt="" style={{ height: "250px", width: "250px" }} />
        ))}
      </div>
    </React.Fragment>
  );
};

export default ImageUpload;
