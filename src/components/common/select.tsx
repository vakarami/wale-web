import React from "react";

interface Props {
  name: string;
  label: string;
  value: string;
  error: string | undefined;
  options: string[];
  onChange: (e:React.ChangeEvent<HTMLSelectElement>)=>void
}

const Select: React.FC<Props> = ({ name, label, options, error, ...rest }) => {
  return (
    <div className="form-group mt-2">
      <label htmlFor={name}>{label}</label>
      <select
        name={name}
        id={name}
        {...rest}
        className="form-control text-capitalize"
      >
        <option value="" />
        {options.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Select;
