import React from "react";

interface Props {
  name: string;
  type: string;
  value: string;
  label: string;
  error: string;
  onChange: (e:React.ChangeEvent<HTMLInputElement>)=>void
}
const Input: React.FC<Props> = ({ name, label, error, ...rest }) => {
  return (
    <div className="form-group mt-2">
      <label htmlFor={name}>{label}</label>
      <input {...rest} name={name} id={name} className="form-control" />
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Input;
