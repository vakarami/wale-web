import React from "react";
import { Link } from "react-router-dom";
import { setPageTitle } from "../../utils/general";

const AdNotFound: React.FC = () => {
  setPageTitle("Ad Not Found")
  return (
    <div className="my-5">
      <h2>Ad Not Found</h2>
      <Link to="/ads" className="btn btn-secondary my-3">
        Back to All Ads
      </Link>
    </div>
  );
};

export default AdNotFound;
