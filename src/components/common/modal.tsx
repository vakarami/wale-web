import React from 'react';

interface Props {
  buttonTitle?: string;
  body: string | JSX.Element | undefined;
  modalTitle?: string;
  action?: React.MouseEventHandler<HTMLButtonElement>;
  actionTitle?: string;
  name: string;
  fullscreen?: string;
}
const Modal: React.FC<Props> = ({buttonTitle, body, modalTitle,action, actionTitle, name, fullscreen }) => {
  return ( <React.Fragment>
  
{buttonTitle && <button type="button" className="btn btn-danger m-2" data-bs-toggle="modal" data-bs-target={"#"+name}>
  {buttonTitle}
</button>}

<div className="modal fade" id={name} tabIndex={-1} aria-labelledby="nameLabel" aria-hidden="true">
  <div className={"modal-dialog "+fullscreen}>
    <div className="modal-content">
      <div className="modal-header">
        {modalTitle && <h5 className="modal-title" id="nameLabel">{modalTitle}</h5>}
        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div className="modal-body">
        {body}
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        {actionTitle && <button type="button" onClick={action} data-bs-dismiss="modal" className="btn btn-danger">{actionTitle}</button>}
      </div>
    </div>
  </div>
</div>
  </React.Fragment> );
}
 
export default Modal;
           
           
           
        