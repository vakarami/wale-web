import React, { Component } from "react";
import { Location } from "history";
import Joi, { SchemaMap } from "joi";
import Input from "./input";
import Select from "./select";
import Textarea from "./textarea";

export interface FormState {
  data: { [index: string]: any };
  errors: { [index: string]: string };
}

export interface FormProps {
  location: Location;
}
abstract class Form<P extends FormProps, S extends FormState> extends Component<P, S> {
  doSubmit() {}
  state = {
    data: {} as { [index: string]: string },
    errors: {} as { [index: string]: string },
  } as S;
  schema: SchemaMap<any> | undefined;

  validate = () => {
    const options = { abortEarly: false, allowUnknown: true };
    const { error } = Joi.object(this.schema).validate(this.state.data, options);
    if (!error) return undefined;

    const errors = {} as { [index: string]: string };
    for (let item of error.details) errors[item.path[0]] = item.message;
    return errors;
  };

  validateProperty = ({ name, value }: { name: string; value: string }) => {
    if (!this.schema) return;
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    if (name === "repassword") {
      obj["password"] = this.state.data["password"];
      schema["password"] = this.schema["password"];
    }
    const { error } = Joi.object(schema).validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;

    this.doSubmit();
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement> | React.ChangeEvent<HTMLTextAreaElement>) => {
    const errors = { ...this.state.errors };
    const input = event.currentTarget;
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];

    const data = { ...this.state.data };
    data[input.name] = input.value;

    this.setState({ data, errors });
  };

  renderButton(label: string) {
    return (
      <button disabled={this.validate() !== undefined} className="btn btn-primary mt-2">
        {label}
      </button>
    );
  }

  renderSelect(name: string, label: string, options: string[]) {
    const { data, errors } = this.state;

    return <Select name={name} value={data[name]} label={label} options={options} onChange={this.handleChange} error={errors[name]} />;
  }

  renderInput(name: string, label: string, type = "text") {
    const { data, errors } = this.state;
    return <Input type={type} name={name} value={data[name]} label={label} onChange={this.handleChange} error={errors[name]} />;
  }

  renderTextarea(name: string, label: string) {
    const { data, errors } = this.state;
    return <Textarea name={name} value={data[name]} label={label} onChange={this.handleChange} error={errors[name]} />;
  }
}

export default Form;
