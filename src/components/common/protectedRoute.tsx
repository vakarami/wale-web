import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isLogin } from "../../services/authService";

interface Props{
  component: typeof React.Component | React.FC,
  path: string
}

const ProtectedRoute: React.FC<Props> = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isLogin())
          return (
            <Redirect
              to={{
                pathname: "/user/login",
                state: props.location ,
              }}
            />
          );
        return <Component {...props} />
      }}
    />
  );
};

export default ProtectedRoute;
