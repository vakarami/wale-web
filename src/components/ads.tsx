import React, { Component } from "react";
import { Location } from "history";
import InfiniteScroll from "react-infinite-scroll-component";
import AdCard from "./adCard";
import { getAll } from "../services/adService";
import { toast } from "react-toastify";
import withLoading from "./common/withLoading";
import { AdType } from "../common/types";
import { RouteComponentProps } from "react-router-dom";
import { setPageTitle } from "../utils/general";

interface State {
  adList?: AdType[];
  hasMore: boolean;
  title: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  onLoadingDone: Function;
  onLoading: Function;
  isLoading: () => boolean;
  location: Location;
}

interface MatchParams {
  id: string;
}

const pageSize = 20;

class Ads extends Component<Props, State> {
  state = {
    adList: [] as AdType[],
    hasMore: true,
    title: "All Ads",
  };

  async componentDidUpdate(prevProps: Props) {
    const prevLocation = prevProps.location;
    const currentLocation = this.props.location;
    if (prevLocation.pathname !== currentLocation.pathname || prevLocation.search !== currentLocation.search) {
      //update if path change from all ads to my ads or reverse
      this.loadAds();
    }
  }

  async componentDidMount() {
    await this.loadAds();
  }

  async loadAds(refresh = true) {
    if (this.props.isLoading()) {
      return; //already loading
    }
    try {
      this.props.onLoading();
      const { data } = await getAll(this.getPagingQuery(refresh));
      this.setData(data, refresh);
    } catch (error) {
      toast.warn("error in getting ads");
    }
    this.props.onLoadingDone();
  }

  setData(data: AdType[] | undefined, refresh: boolean) {
    if (data) {
      this.setState({ adList: refresh ? data : [...this.state.adList, ...(data as AdType[])] });
      this.setState({ hasMore: data?.length >= pageSize });
    }
  }

  getPagingQuery(refresh: boolean): string {
    const search = this.props.location.search;
    let query = search ? search + "&" : "?";
    query += `limit=${pageSize}`;
    if (!refresh) {
      query += `&skip=${this.state.adList.length}`;
    }
    this.setDocumentTitle(search);
    return query;
  }

  private setDocumentTitle(search: string) {
    if (search) {
      const params = search.split("=");
      if (params[0].includes("category")) {
        this.setState({ title: `All Ads in: ${search.split("=")[1]} category` });
      } else {
        this.setState({ title: "All ads having: " + search.split("=")[1] });
      }
    } else {
      this.setState({ title: "All Ads" });
    }
  }

  render() {
    setPageTitle(this.state.title);
    return (
      <div>
        <h2 className="mb-3">{this.state.title}</h2>
        {this.noAdAvailable()}
        {this.state.adList &&
          this.renderScroll(
            <div className="row gx-3 gy-2 card-group">
              {this.state.adList.map((ad) => (
                <AdCard key={ad._id} details={ad} />
              ))}
            </div>
          )}
      </div>
    );
  }

  renderScroll(wrappedElement: JSX.Element) {
    return (
      <InfiniteScroll
        dataLength={this.state.adList?.length}
        next={() => this.loadAds(false)}
        hasMore={this.state.hasMore}
        loader={<div />}
        endMessage={
          <p className="text-center">
            <b>No More Ads available now</b>
          </p>
        }
        refreshFunction={() => this.loadAds(true)}
        pullDownToRefresh
        pullDownToRefreshThreshold={50}
        pullDownToRefreshContent={<h3 className="text-center">&#8595; Pull down to refresh</h3>}
        releaseToRefreshContent={<h3 className="text-center">&#8593; Release to refresh</h3>}
      >
        {wrappedElement}
      </InfiniteScroll>
    );
  }

  noAdAvailable() {
    const noAdAvailable = !this.props.isLoading() && !this.state.adList?.length && (
      <div className="alert alert-warning my-5" role="alert">
        There is no ad available
      </div>
    );
    return noAdAvailable;
  }
}

export const SimpleAds = Ads; //alows inheritance
export default withLoading(Ads);
