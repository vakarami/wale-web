import React, { Component } from "react";
import { History } from "history";
import { Link, RouteComponentProps } from "react-router-dom";
import { getRelativeTime, setPageTitle } from "../utils/general";
import { deleteAd, getById } from "../services/adService";
import { toast } from "react-toastify";
import AdNotFound from "./common/adNotFound";
import { getCurrentUser } from "../services/authService";
import Modal from "./common/modal";
import withLoading from "./common/withLoading";
import { AdType, emptyAd, UserType } from "../common/types";

interface State {
  ad?: AdType;
}
interface Props extends RouteComponentProps<MatchParams> {
  onLoadingDone: Function;
  onLoading: Function;
  isLoading: () => boolean;
  history: History;
}

interface MatchParams {
  id: string;
}

const baseUrl = process.env.REACT_APP_BACKEND;

class AdDetails extends Component<Props, State> {
  state = {
    ad: emptyAd,
  };
  async componentDidMount() {
    this.props.onLoading();
    const response = await getById(this.props.match.params.id);
    this.props.onLoadingDone();
    if (!response.error) {
      this.setState({ ad: response.data as AdType });
    }
  }

  deleteHandle = async () => {
    this.props.onLoading();
    const response = await deleteAd(this.state.ad);
    if (response.error) {
      toast.warn("can't delete the ad: " + response.error);
    } else {
      this.props.history.replace("/");
    }
    this.props.onLoadingDone();
  };

  render() {
    if(this.props.isLoading()){
      return <div/>
    }
    const ad = this.state.ad;
    if (!ad.title) {
      return <AdNotFound />;
    }
    setPageTitle(ad.title);
    const owner:UserType = ad.owner as UserType;
    return (
      <div className="row w-100">
        <div className="col-lg-7">
          <div className="row">
            <h1 className="mt-2 col-11">{ad.title}</h1>
            <div className="col-1">{this.renderActions(ad)}</div>
          </div>
          <Modal name="slide" body={this.renderSlideShow(ad.photos, "fullscreen")} fullscreen="modal-fullscreen-md-down" />
          <h4>
            {ad.currency + " "}
            {ad.price}
          </h4>
          <small className="text-muted">
            {getRelativeTime(new Date(ad.createdAt))}
            {" in " + ad.city}
            <Link className="btn btn-secondary btn-sm ms-2 text-capitalize" to={"/ads?category=" + ad.category}>
              {ad.category}
            </Link>
          </small>
          <h3 className="mt-2">Description</h3>
          <p style={{ whiteSpace: "pre-line" }}>{ad.description}</p>
          <h4 className="mt-2">Contact Details</h4>
          <p>{ad.contact}</p>
          <p>
            Posted by <Link to={"/user/ads/" + owner._id}>{owner.name}</Link>{" "}
          </p>
          <p>
            View All <Link to={"/user/ads/" + owner._id}>User Ads</Link>{" "}
          </p>
        </div>
        <div className="col-lg-5">{this.renderSlideShow(ad.photos, "page")}</div>
      </div>
    );
  }

  renderActions(ad: AdType) {
    const user = getCurrentUser();
    if (user?._id === (ad.owner as UserType)._id) {
      return (
        <React.Fragment>
          <div className="dropdown">
            <button className="btn " data-bs-toggle="dropdown" aria-expanded="false">
              {/* kebab menu item */}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-three-dots-vertical"
                viewBox="0 0 16 16"
              >
                <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"></path>
              </svg>
            </button>
            <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenu2">
              <li>
                <Link to={`/ads/${ad._id}/edit`} className="dropdown-item" type="button">
                  Edit
                </Link>
              </li>
              <li>
                <Link to="#delete" data-bs-toggle="modal" className="dropdown-item">
                  Delete
                </Link>
              </li>
            </ul>
          </div>
          <Modal
            modalTitle="Delete the Ad?"
            name="delete"
            action={this.deleteHandle}
            actionTitle="Delete"
            body="Are you sure, this action is not reversable"
          />
        </React.Fragment>
      );
    }
  }

  renderSlideShow(photos: string[], content: string) {
    const id = "adSlideShow" + content;
    if (photos?.length > 1)
      return (
        <div id={id} className="carousel slide" data-bs-ride="carousel" data-bs-interval="false" data-bs-pause="hover" style={{ maxWidth: "600px" }}>
          <div className="carousel-indicators">
            {photos.map((photo, i) => (
              <button
                type="button"
                key={i}
                data-bs-target={"#" + id}
                className={i === 0 ? "active" : ""}
                aria-current={i === 0}
                data-bs-slide-to={i}
                aria-label={"Slide " + i}
              ></button>
            ))}
          </div>
          <Link to="#slide" data-bs-toggle="modal">
            <div className="carousel-inner">
              {photos.map((photo, i) => (
                <div key={i} className={"carousel-item " + (i === 0 ? "active" : "")}>
                  <img src={baseUrl + "/" + photo} className="d-block w-100" alt="" />
                </div>
              ))}
            </div>
          </Link>
          <button className="carousel-control-prev" type="button" data-bs-target={"#" + id} data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button className="carousel-control-next" type="button" data-bs-target={"#" + id} data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      );
    if (photos?.length === 1) {
      return (
        <Link to="#slide" data-bs-toggle="modal">
          <img src={baseUrl + "/" + photos[0]} className="d-block w-100" alt="" />
        </Link>
      );
    }
  }
}

export default withLoading(AdDetails);
