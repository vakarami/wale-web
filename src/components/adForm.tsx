import React from "react";
import Joi from "joi";
import { getById, post, update } from "../services/adService";
import Form, { FormProps, FormState } from "./common/form";
import { toast } from "react-toastify";
import AdNotFound from "./common/adNotFound";
import { getCurrentUser } from "../services/authService";
import ImageUpload from "./common/imageUpload";
import withLoading from "./common/withLoading";
import { AdType, emptyAd, UserType } from "../common/types";
import { History } from "history";
import { RouteComponentProps } from "react-router-dom";
import { setPageTitle } from "../utils/general";

interface State extends FormState {
  data: AdType;
  errors: { [index: string]: string };
  new: boolean;
  notFound: boolean;
  invalid: boolean;
}

interface Props extends FormProps, RouteComponentProps<MatchParams> {
  history: History;
  onLoadingDone: () => void;
  onLoading: () => void;
}

interface MatchParams {
  id: string;
}

const baseUrl = process.env.REACT_APP_BACKEND;
const categories = ["car", "house", "mobile", "laptop", "electronics", "home", "wood"];
class AdForm extends Form<Props, State> {
  state = {
    data: emptyAd,
    new: true,
    notFound: false,
    invalid: false,
    //todo get from server
    currencies: ["USD", "EUR", "SGD"],
    errors: {},
  };
  schema = {
    _id: Joi.string().allow(null, ""),
    title: Joi.string().min(3).max(50).required().label("Title"),
    description: Joi.string().min(10).max(2000).required().label("Description"),
    price: Joi.number().required().label("Price"),
    currency: Joi.string().required().label("Currency"),
    category: Joi.string().required().label("Category"),
    city: Joi.string().required().label("City"),
    contact: Joi.string().required().label("Contact"),
  };

  async componentDidUpdate(prevProps: Props) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      //update if path change from edit to new or reverse
      this.populateAd();
    }
  }

  async componentDidMount() {
    await this.populateAd();
    if (this.state.invalid) {
      this.props.history.replace("/");
    }
  }

  async populateAd() {
    if (this.props.location.pathname.endsWith("new")) {
      this.setState({
        data: emptyAd,
        new: true,
        notFound: false,
        invalid: false,
      });
    } else {
      await this.loadAdForEdit();
    }
    this.props.onLoadingDone();
  }

  async loadAdForEdit() {
    this.props.onLoading();
    const user = getCurrentUser();
    const response = await getById(this.props.match.params.id);
    if (response.data) {
      const ad = response.data;
      if (!user || (ad.owner as UserType)._id !== user._id) {
        return this.setState({ invalid: true });
      }
      this.setState({ data: ad, new: false });
    } else {
      toast.warn("Can't load this ad");
      this.setState({ invalid: true });
    }
  }

  doSubmit = async () => {
    var response;
    if (this.state.new) {
      response = await post(this.state.data);
    } else {
      response = await update(this.state.data);
    }
    if (response.error) toast.error("Unable to save the changes: " + response.error);
    else {
      toast.success(`New Ad ${this.state.new ? "posted" : "updated"} successfully`);
      this.props.history.push("/ads");
    }
  };
  uploadHandle = (photos: string[]) => {
    const adData = this.state.data;
    adData.photos = photos;
    this.setState({ data: adData });
  };
  goBack = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    this.props.history.goBack();
  };
  render() {
    if (this.state.notFound) {
      return <AdNotFound />;
    }
    setPageTitle(this.state.new ? "Create new Ad in" : "Edit your Ad");
    const photos = this.state.data.photos.map((photo) => baseUrl + "/" + photo);
    return (
      <div>
        <h1>{this.state.new ? "New Free" : "Edit Your"} Ad</h1>
        <form onSubmit={this.handleSubmit} style={{ maxWidth: "640px" }}>
          <ImageUpload onUpload={this.uploadHandle} photos={photos} />
          {this.renderInput("title", "Title")}
          {this.renderSelect("category", "Category", categories)}
          {this.renderInput("price", "Price")}
          {this.renderSelect("currency", "Currency", this.state.currencies)}
          {this.renderInput("city", "City")}
          {this.renderInput("contact", "Contact Details (Phone Number or Email)")}
          {this.renderTextarea("description", "Description")}
          {this.renderButton(this.state.new ? "Submit" : "Save")}
          <button className="btn btn-secondary mt-2 ms-2" onClick={this.goBack}>
            Cancel
          </button>
        </form>
      </div>
    );
  }
}

export default withLoading(AdForm);
