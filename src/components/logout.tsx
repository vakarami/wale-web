import React, { useEffect } from "react";
import { logout } from "../services/authService";
import { toast } from "react-toastify";
import withLoading from "./common/withLoading";

const Logout: React.FC = () => {
  useEffect(() => {
    async function logoutUser() {
      const result = await logout();
      if (result) window.location.href = "/"; //to reload the page
      else {
        toast.error("can't logout, try later");
        setTimeout(() => {
          window.location.href = "/"; //to reload the page
        }, 2000);
      }
    }
    logoutUser();
  });

  return <div />;
};

export default withLoading(Logout);
