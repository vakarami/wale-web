export type AdType = {
  _id?: string,
  title: string,
  description: string,
  price: string|number,
  currency: Currency,
  category: string,
  createdAt: string | Date,
  owner: string | UserType,
  city: string,
  contact: string,
  photos: string[],
}

export enum Currency {
  "USD", "SGD", "EUR"
}

export const emptyAd: AdType = {
  _id: "",
  title: "",
  description: "",
  price: "",
  currency: Currency.USD,
  category: "",
  createdAt: new Date(),
  owner: "",
  city: "",
  contact: "",
  photos: [],
};

export type UserType = {
  _id?: string,
  name: string,
  email: string,
  password: string,
  repassword: string,
}

export const emptyUser: UserType={
  name: "",
  email: "",
  password: "",
  repassword: "",
}