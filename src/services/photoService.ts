import http from "./httpService";

interface PhotoResponse{
  data?: string[],
  error?: string,
}

export async function upload(photos: File[], onUploadProgress: (e:any)=>void):Promise<PhotoResponse> {
  const formData = new FormData();
  photos.map((photo) => formData.append("photos", photo));
  try {
    return await http.post("/photos/upload", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
      onUploadProgress,
    });
  } catch (error) {
    return { error: error.response};
  }
}
