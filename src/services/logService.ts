function init() {}

function log(error: string) {
  console.error(error);
}

const methods = {
  init,
  log,
};
export default methods;
