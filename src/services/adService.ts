import { AdType } from "../common/types";
import http from "./httpService";


interface AdResponse {
  data?: AdType;
  error?: string;
}

interface AdArrayResponse {
  data?: AdType[];
  error?: string;
}
export async function getAll(queryString: string): Promise<AdArrayResponse> {
  try {
    return await http.get("/ads" + queryString);
  } catch (error) {
    return { error: error.response };
  }
}

export async function getByUser(userId: string, queryString: string): Promise<AdArrayResponse> {
  try {
    return await http.get("/user/ads/" + userId+queryString);
  } catch (error) {
    return { error: error.response };
  }
}

export async function getById(id: string): Promise<AdResponse> {
  try {
    return await http.get("/ad/" + id);
  } catch (error) {
    return { error: error.response };
  }
}

export async function post(postedAd: AdType): Promise<AdResponse> {
  try {
    delete postedAd._id;
    return await http.post("/ads/new", postedAd);
  } catch (error) {
    return { error: error.response.data };
  }
}

export async function update(postedAd: AdType): Promise<AdResponse> {
  try {
    return await http.patch("/ad/" + postedAd._id, postedAd);
  } catch (error) {
    return { error: error.response.data };
  }
}

export async function deleteAd(ad: AdType): Promise<AdResponse> {
  try {
    return await http.delete("/ad/" + ad._id);
  } catch (error) {
    return { error: error.response.data };
  }
}
