import { UserType } from "../common/types";
import http from "./httpService";

const tokenKey = "walle-token";

http.setJwt(getToken());

export async function login( email: string, password:string ):Promise<boolean> {
  localStorage.removeItem(tokenKey);
  const data = { email, password };
  try {
    const { data: userdata } = await http.post("/user/login", data);
    if (userdata) {
      localStorage.setItem(tokenKey, JSON.stringify(userdata));
      return true;
    }
  } catch (error) {
  }
  return false;
}

export async function register( user: UserType ):Promise<{error?:string}> {
  localStorage.removeItem(tokenKey);
  try {
    const { data, status } = await http.post("/user/new", user);
    if(status!==201){
      return {error: data}
    }
    if (data) {
      localStorage.setItem(tokenKey, JSON.stringify(data));
    }
  } catch (error) {
    return {error: error.response.data}
  }
  return {};
}

export async function getUser( _id: string ):Promise<UserType | undefined> {
  try {
    const { data } = await http.get("/user/"+_id);
    return data as UserType;
  } catch (error) {
  }
  return undefined;
}

export async function logout(): Promise<boolean> {
  try {
    await http.post("/user/logout");
    localStorage.removeItem(tokenKey);
    return true;
  } catch (error) {
    return false;
  }
}

export function isLogin(): boolean {
  return localStorage[tokenKey] !== undefined;
}

export function getToken(): string | null {
  if (isLogin()) return JSON.parse(localStorage[tokenKey]).token;
  return null;
}

export function getCurrentUser(): UserType | null {
  if (isLogin()) return JSON.parse(localStorage[tokenKey]).user;
  return null;
}
