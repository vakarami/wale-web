import React from "react";
import { Redirect, Route, Switch } from "react-router";
import { ToastContainer } from "react-toastify";
import AdDetails from "./components/adDetails";
import Ads from "./components/ads";
import NavBar from "./components/navBar";
import AdForm from "./components/adForm";
import Login from "./components/login";
import Logout from "./components/logout";
import ProtectedRoute from "./components/common/protectedRoute";
import Profile from "./components/profile";
import UserAds from "./components/userAds";
import { isLogin } from "./services/authService";
import RegisterForm from './components/registerForm';

function App() {
  return (
    <React.Fragment>
      <NavBar />
      <ToastContainer autoClose={5000} closeOnClick={false} rtl={false} />
      <div className="container">
        <Switch>
          <ProtectedRoute path="/ads/new" component={AdForm} />
          <ProtectedRoute path="/ads/:id/edit" component={AdForm} />
          <Route path="/ads/:id" component={AdDetails} />
          <Route path="/ads" component={Ads} />
          {!isLogin() && <Route path="/user/register" component={RegisterForm} />}
          {!isLogin() && <Route path="/user/login" component={Login} />}
          {isLogin() && <Route path="/user/logout" component={Logout} />}
          <ProtectedRoute path="/user/me" component={Profile} />
          <Route path="/user/ads/:id" component={UserAds} />
          <Redirect from="/" to="/ads" />
        </Switch>
      </div>
    </React.Fragment>
  );
}

export default App;
