### STAGE 1: Build ###
FROM node:15.6.0-alpine as build

WORKDIR /usr/src/app

ENV NODE_ENV production

COPY package*.json ./
RUN npm install --production

COPY . .

RUN npx tsc
COPY package.json ./dist/
COPY .env.production ./dist/
COPY public ./dist/public

#build folder goes under dist
RUN npm run build 

### STAGE 2: Production Environment ###
FROM nginx:1.21.0-alpine
COPY --from=build /usr/src/app/dist/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
